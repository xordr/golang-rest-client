function accept() {

    var xhr = new XMLHttpRequest();
    var devices = document.getElementById('devices');

    xhr.onload = function () {

        var json = JSON.parse(this.responseText);
        var data = "";

        for (var i = 0; i < json.length; i++) {
            data += '<u><b>ID:</b></u> ' + json[i]["id"] +
                ' | <u><b>Device Name:</b></u> ' + json[i]["deviceName"] + "</br>";
        }

        document.getElementById('devices').innerHTML = data;
        document.getElementById('certain_device').innerHTML =
            '<div id="choose">' +
            '<h1>Enter the device number:</h1>' +
            '</div>' +
            '<form onsubmit="getCertainDevice();return false;">' +
            '<input type="text" id="text_field">' +
            '<input type="submit">' +
            '</form>';
    };


    xhr.open('GET', 'http://localhost:8080/devices', true);
    xhr.send();
}

function getCertainDevice() {

    var id = document.getElementById('text_field').value;
    var xhr = new XMLHttpRequest();

    xhr.onload = function () {

        var json = JSON.parse(this.responseText);
        var data = "";

        if (Object.keys(json).length === 0) {
            data += 'NO DEVICE WITH ID = ' + id + '!';
        } else {
            data += '<u><b>ID:</b></u> ' + json["id"] +
                '</br><u><b>Device Name:</b></u> ' + json["deviceName"] +
                '</br><u><b>Hardware Address:</b></u> ' + json["hardwareAddress"] +
                '</br><u><b>MTU:</b></u> ' + json["mtu"] +
                '</br><u><b>Flags:</b></u> ' + json["flags"];
        }
        document.getElementById('certain_device_spec').innerHTML = data;

    };

    xhr.open('GET', 'http://localhost:8080/devices/' + id, true);
    xhr.send();
}